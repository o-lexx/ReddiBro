//
//  RedditFeedViewController.swift
//  ReddiBro-iOS
//
//  Created by Alexey Lizenko on 8/27/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import UIKit


class RedditFeedTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var authorLabel: UILabel?
    @IBOutlet weak var raitingLabel: UILabel?
    @IBOutlet weak var timeLabel: UILabel?
    @IBOutlet weak var thumbnailView: NetImageView?
    
    override func prepareForReuse() {
         super.prepareForReuse()
        thumbnailView?.cancelLoading()
    }
}


class RedditLoadingTableFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
}


protocol RedditFeedDelegate {
    func viewController(_ viewController: UIViewController, didSelectEntity entity: RedditEntity)
}


class RedditFeedViewController: UITableViewController {
    
    enum Identifier {
        static let feedCell = "RedditFeedCell"
        static let loadingFooter = "RedditLoadingFooter"
    }
    
    var feedProvider: FeedProvider?
    var delegate: RedditFeedDelegate?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.rowHeight = UITableViewAutomaticDimension;
        tableView?.estimatedRowHeight = 70.0

        let loadingFooterNib = UINib(nibName: "RedditLoadingTableFooterView", bundle: nil)
        tableView?.register(loadingFooterNib, forHeaderFooterViewReuseIdentifier: Identifier.loadingFooter)
        
        let longPressGesture = UILongPressGestureRecognizer.init(target: self, action: #selector(longPressAction))
        self.tableView.addGestureRecognizer(longPressGesture)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        feedProvider?.fetchContent { _ in
            self.tableView?.reloadData()
        }
    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedProvider?.entities.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.feedCell) as! RedditFeedTableViewCell
        
        if let redditEntity = feedProvider?.entities[indexPath.row] {
            cell.titleLabel?.text = redditEntity.title
            cell.authorLabel?.text = redditEntity.author
            cell.raitingLabel?.text = "\(redditEntity.commentsCount)"
            cell.timeLabel?.text = redditEntity.createdDate.localizedTimePeriodFromNow
            
            if let thumbnail = redditEntity.thumbnail {
                cell.thumbnailView?.loadImage(withURL: thumbnail, placeholder: UIImage(named: "placeholder")!)
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? RedditFeedTableViewCell {
            cell.thumbnailView?.cancelLoading()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let redditEntity = feedProvider?.entities[indexPath.row] {
            self.delegate?.viewController(self, didSelectEntity: redditEntity)
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: -
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return tableView.dequeueReusableHeaderFooterView(withIdentifier: Identifier.loadingFooter)
    }
    
    override func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if let footerView = view as? RedditLoadingTableFooterView {
            footerView.activityIndicator?.startAnimating()
            if (feedProvider?.isMoreAvailable ?? false) {
                feedProvider?.fetchContent({ _ in
                    self.tableView.reloadData()
                })
            }
        }
    }
    
    // MARK: -
    
    @objc func longPressAction(_ sender: UILongPressGestureRecognizer) {
        let location = sender.location(in: tableView)
        guard sender.state == .began else {
            return
        }
        
        if let indexPath = tableView.indexPathForRow(at: location), let redditEntity = feedProvider?.entities[indexPath.row] {
            
            let alertController = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction.init(title: "Add image to camera roll", style: .default, handler: {_ in
                self.addImageToPhotoLibrary(fromEntity: redditEntity)
            }))
            alertController.addAction(UIAlertAction.init(title: "Open image", style: .default, handler: {_ in
                self.delegate?.viewController(self, didSelectEntity: redditEntity)
            }))
            alertController.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

            present(alertController, animated: true, completion: nil)
        }
    }
    
    func addImageToPhotoLibrary(fromEntity redditEntity: RedditEntity) {
        let manager = NetworkManager()
        if let image = redditEntity.image {

            let progressController = UIAlertController(title: nil, message: "In progress…", preferredStyle: .alert)
            self.present(progressController, animated: true)
            
            manager.fetchImage(image, completion: { (imageResult) in
                print("image: \(imageResult)")
                if let image = imageResult.value {
                    let photoHelper = PhotoAlbumHelper()
                    photoHelper.save(image: image, withCompletion: {_ in
                        progressController.dismiss(animated: true)
                    })
                }
                else {
                    progressController.dismiss(animated: true)
                }
            })
        }
    }
}
