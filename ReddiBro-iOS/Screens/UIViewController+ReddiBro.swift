//
//  UIViewController+ReddiBro.swift
//  ReddiBro-iOS
//
//  Created by Alexey Lizenko on 8/27/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import UIKit


extension UIViewController {
    class func instantiateViewController() -> Self? {
        let defaultName = String(describing: self)
        return instantiateViewController(withStoryboardName: defaultName)
    }

    class func instantiateViewController(withStoryboardName storyboardName: String, identifier: String? = nil) -> Self? {
        return instantiateViewControllerTemplate(storyboardName: storyboardName, identifier: identifier, type: self)
    }

    private class func instantiateViewControllerTemplate<T>(storyboardName: String, identifier: String? = nil, type: T.Type) -> T? {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let viewController = storyboard.instantiateInitialViewController() as? T

        return viewController
    }
}
