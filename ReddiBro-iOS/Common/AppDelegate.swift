//
//  AppDelegate.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/5/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var networkManager = NetworkManager()
    
    var modules: [AppModule]

    override init() {
        modules = [MainAppModule.init(networkManager: networkManager)]
    }
}

