//
//  PhotoAlbumHelper.swift
//  ReddiBro-iOS
//
//  Created by Alexey Lizenko on 8/31/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import UIKit

class PhotoAlbumHelper: NSObject {
    
    typealias PhotoAlbumHelperCompetion = (Result<UIImage>) -> ()
    
    private var completion: PhotoAlbumHelperCompetion?
    
    
    func save(image: UIImage, withCompletion inCompletion: @escaping PhotoAlbumHelperCompetion) {
        completion = inCompletion
        
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc private func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            completion?(Result.failure(error as NSError))
            
        }
        else {
            completion?(Result(image))
        }
        completion = nil
    }
}
