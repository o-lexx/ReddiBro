//
//  AppModule.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/5/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import UIKit

protocol AppModule : UIApplicationDelegate {
    
}
