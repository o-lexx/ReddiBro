//
//  MainRouter.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import UIKit
import SafariServices

class MainRouter: NSObject {
    var networkManager: NetworkManager
    var window: UIWindow
    var mainNavigationController: UINavigationController?
    
    public init(withWindow inWindow: UIWindow, networkManager inNetworkManager: NetworkManager) {
        window = inWindow
        networkManager = inNetworkManager
    }
    
    public func setupViewStack() {
        mainNavigationController = UINavigationController()
        
        if let feedController = RedditFeedViewController.instantiateViewController() {
            feedController.feedProvider = FeedProvider(withSubreddit: .top, networkManager: networkManager)
            feedController.delegate = self
            mainNavigationController?.pushViewController(feedController, animated: false)
        }
        window.rootViewController = mainNavigationController
    }
}

extension MainRouter: RedditFeedDelegate {
    func viewController(_ viewController: UIViewController, didSelectEntity entity: RedditEntity) {
        if let imageURL = entity.image {
            let safariController = SFSafariViewController(url: imageURL)
            safariController.delegate = self
            mainNavigationController?.present(safariController, animated: true, completion: nil)
        }
    }
}

extension MainRouter: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true)
    }
}
