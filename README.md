# ReddiBro

Current project contains 2 targets: for iOS and Mac OS.
Shared components (network e.g.) are user for unification.

As main patters MVC with Router and App Modules was used. 

### App Modules

Gives ability to keep AppDelegate small and place different parts of logic in different classes, like crashlitic, logger and other tools

### Routers

Gives ability to put iPhone/iPad specific navigation in different classes and separate it from main UI and business logic

### MVC

In all other terms application utilizes MVC paradigm
