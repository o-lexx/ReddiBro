//
//  ImagePreviewViewController.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/13/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Cocoa


class ImagePreviewViewController: NSViewController {
    var imageURL: URL? = nil {
        didSet {
            self.imageView.image = nil
            if let url = imageURL {
                loadImage(withURL: url)
            }
        }
    }
    
    var imageData: Data? = nil
    var loadingTask: URLSessionDataTask?

    @IBOutlet weak var saveButton: NSButton!
    @IBOutlet weak var imageView: NetImageView!
    @IBOutlet weak var progressIndicator: NSProgressIndicator!
    
    override func viewWillAppear() {
        super.viewWillAppear()
    }
    
    func loadImage(withURL url: URL) {
        self.progressIndicator.isHidden = false
        self.progressIndicator.startAnimation(self)

        loadingTask = URLSession.shared.dataTask(with: url, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            if let dataVal = data {
                self.imageData = data
                let image = NSImage.init(data: dataVal)
                DispatchQueue.main.async {
                    self.imageView.image = image
                    
                    self.progressIndicator.stopAnimation(self)
                    self.progressIndicator.isHidden = true
                    self.saveButton.isEnabled = true
                }
            }
            else {
                self.progressIndicator.stopAnimation(self)
            }
        })
        loadingTask?.resume()
    }
    
    @IBAction func save(_ sender: Any) {
        let savePanel = NSSavePanel()
        
        guard let imageURL = self.imageURL else {
            return
        }
        guard let imageData = self.imageData else {
            return
        }
        
        savePanel.allowedFileTypes = [imageURL.pathExtension]
        if let window = self.view.window {
            savePanel.beginSheetModal(for: window, completionHandler: { (result) in
                if result == NSFileHandlingPanelOKButton {
                    if let saveURL = savePanel.url {
                        do {
                            try imageData.write(to: saveURL)
                        }
                        catch {
                            print("Unable to save file")
                        }
                    }
                }
            })
        }
    }
}
