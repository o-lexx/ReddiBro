//
//  RedditFeedWindowController.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Cocoa

class RedditFeedWindowController: NSWindowController{
    var provider: FeedProvider!
    weak var delegate: RoutingDelegate?
    
    var mainViewController: RedditFeedTableViewController? {
        return self.contentViewController as! RedditFeedTableViewController?
    }
    
    
    class func instantiateWindowController(withProvider provider: FeedProvider, delegate: RoutingDelegate) -> RedditFeedWindowController {
        let storyboard = NSStoryboard.init(name: "RedditFeed", bundle: nil)
        let windowController = storyboard.instantiateInitialController() as! RedditFeedWindowController
        windowController.provider = provider
        windowController.mainViewController?.provider = provider
        windowController.mainViewController?.delegate = delegate
        
        return windowController
    }
}
