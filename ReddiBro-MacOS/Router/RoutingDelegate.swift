//
//  RoutingDelegate.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Cocoa

protocol RoutingDelegate: class {
    func didSelecItem(_ item: RedditEntity, source: NSViewController)
    func showPreviewForItem(_ item: RedditEntity, source: NSViewController)
}
