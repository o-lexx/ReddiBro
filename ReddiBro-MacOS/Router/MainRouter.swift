//
//  MainRouter.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Cocoa

class MainRouter: RoutingDelegate {
    private var networkManager: NetworkManager
    private var provider: FeedProvider
    private var feedWindowController: RedditFeedWindowController?
    private var previewWindowController: ImagePreviewWindowController?
    
    public init(withNetworkManager networkManager: NetworkManager) {
        self.networkManager = networkManager
        self.provider = FeedProvider(withSubreddit: .top, networkManager: networkManager)
    }
    
    public func setupWindowStack() {
        assert(feedWindowController == nil)
        
        feedWindowController = RedditFeedWindowController.instantiateWindowController(withProvider: provider, delegate: self)
        feedWindowController?.showWindow(nil)
    }
    
    public func showWindow() {
        assert(feedWindowController != nil)
        
        feedWindowController?.showWindow(nil)
    }
    
    func didSelecItem(_ item: RedditEntity, source: NSViewController) {
        // Do something
    }
    
    func showPreviewForItem(_ item: RedditEntity, source: NSViewController) {
        if let url = item.image {
            if let previewController = previewWindowController {
                previewController.imageURL = url
            }
            else
            {
                previewWindowController = ImagePreviewWindowController.instantiateWindowController(withImageURL: url)
            }
            
            previewWindowController?.showWindow(nil)
        }
    }
}

