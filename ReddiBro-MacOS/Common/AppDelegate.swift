//
//  AppDelegate.swift
//  ReddiBro-MacOS
//
//  Created by Alexey Lizenko on 6/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    // It's easy to add extra modules for logging, analitics setup, etc.
    let modules: [AppModule] = [MainModule()]
    

}

