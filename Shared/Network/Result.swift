//
//  Result.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(NSError)
    
    init(_ result: T, error: NSError? = nil) {
        if let errorVal = error {
            self = .failure(errorVal)
        }
        else {
            self = .success(result)
        }
    }
    
    init(error: NSError?) {
        if let error = error {
            self = .failure(error)
        } else {
            self = .failure(RedditError.unknown as NSError)
        }
    }
    
    var error: NSError? {
        switch self {
        case .failure(let error):
            return error
        default:
            return nil
        }
    }
    
    var value: T? {
        switch self {
        case .success(let success):
            return success
        default:
            return nil
        }
    }

    // MARK:-
    
    func package<B>(ifSuccess: (T) -> B, ifFailure: (NSError) -> B) -> B {
        switch self {
        case .success(let value):
            return ifSuccess(value)
        case .failure(let value):
            return ifFailure(value)
        }
    }
    
    func map<B>(_ transform: (T) -> B) -> Result<B> {
        return flatMap { .success(transform($0)) }
    }
    
    public func flatMap<B>(_ transform: (T) -> Result<B>) -> Result<B> {
        return package(
            ifSuccess: transform,
            ifFailure: Result<B>.failure)
    }

    
}
