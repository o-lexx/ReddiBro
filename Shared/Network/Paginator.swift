//
//  Paginator.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Foundation

struct Paginator {
    let after: String?
    let before: String?
    
    init(before: String? = nil, after: String? = nil) {
        self.before = before
        self.after = after
    }

    init(withResponceDictionary responceDictionary: JSONDictionary) {
        if let data = responceDictionary["data"] as? JSONDictionary {
            let after = data["after"] as? String
            let before = data["before"] as? String
            
            self.before = before
            self.after = after
        }
        else {
            self.before = nil
            self.after = nil
        }
    }

    var parameterDictionary: Dictionary<String, String> {
        get {
            var parameters = Dictionary<String, String>()
            if let afterVal = after {
                parameters["after"] = afterVal
            }
            if let beforeVal = before {
                parameters["before"] = beforeVal
            }
            
            return parameters
        }
    }
}
