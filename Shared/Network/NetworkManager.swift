//
//  NetworkManager.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Foundation

class NetworkManager {
    let baseURL: String = "https://api.reddit.com/"

    enum Subreddit: String {
        case top = "top"
    }
    
    var session = URLSession(configuration: URLSessionConfiguration.default)

    // MARK:-

    private class func GetRequest(with baseURL: String, path: String, parameter: [String:String]) -> URLRequest? {
        let param = parameter.URLQuery
        guard let URL = param.characters.isEmpty ? URL(string:baseURL + path) : URL(string:baseURL + path + "?" + param) else { return nil }
        var request = URLRequest(url: URL)
        request.httpMethod = "GET"
        
        return request
    }

    func fetch(subreddit: Subreddit, paginator: Paginator, completion: @escaping (Result<([RedditEntity], Paginator)>) -> Void) {
        var paramDictionary = Dictionary<String, String>()
        paramDictionary.merge(with: paginator.parameterDictionary);
        
        if let urlRequest = NetworkManager.GetRequest(with: baseURL, path: subreddit.rawValue, parameter: paramDictionary) {
            let task = session.dataTask(with: urlRequest, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
                
                let result = Result(Response(data: data, urlResponse: response), error: error as NSError?)
                        .flatMap(self.responseToData)
                        .flatMap(self.dataToJson)
                        .flatMap(self.jsonToRedditEntity)
                DispatchQueue.main.async {
                    completion(result)
                }
            })
            task.resume()
        }
        else {
            DispatchQueue.main.async {
                completion(Result(error: RedditError.unableToFormRequest as NSError))
            }
        }
    }
}
