//
//  NetworkManager+Parsing_iOS.swift
//  ReddiBro-iOS
//
//  Created by Alexey Lizenko on 8/31/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import UIKit

extension NetworkManager {
    func dataToImage(from data: Data) -> Result<UIImage> {
        if let result = UIImage(data: data) {
            return Result(result)
        }
        else {
            return Result(error: nil)
        }
    }
}
