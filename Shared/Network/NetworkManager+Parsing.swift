//
//  NetworkManager+Parsing.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Foundation

extension NetworkManager {
    func responseToData(from response: Response) -> Result<Data> {
        if !(200..<300 ~= response.statusCode) {
            do {
                let json = try JSONSerialization.jsonObject(with: response.data, options: [])
                if let json = json as? JSONDictionary {
                    return .failure(HTTPError<JSONDictionary>(withCode: response.statusCode, object: json) as NSError)
                }
            } catch {
                print(error)
            }
            if let bodyAsString = String(data: response.data, encoding: .utf8) {
                return .failure(HTTPError<String>(withCode: response.statusCode, object: bodyAsString) as NSError)
            }
            return .failure(HTTPError<Data>(withCode: response.statusCode, object: response.data) as NSError)
        }
        return .success(response.data)
    }

    func dataToJson(from data: Data) -> Result<JSONAny> {
        do {
            if data.count == 0 {
                return Result([:])
            } else {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                return Result(json)
            }
        } catch {
            return Result(error: error as NSError)
        }
    }

    func dataToString(from data: Data) -> Result<String> {
        if data.count == 0 {
            return Result("")
        }
        if let decoded = String(data:data, encoding:.utf8) {
            return Result(decoded)
        }
        return Result(error:RedditError.unableToParseString(data) as NSError)
    }

    func jsonToRedditEntity(from json: JSONAny) -> Result<([RedditEntity], Paginator)> {
        if let jsonDictionary = json as? JSONDictionary {
            if let data = jsonDictionary["data"] as? JSONDictionary {
                if let children = data["children"] as? Array<JSONDictionary> {
                    var result: Array<RedditEntity> = []
                    for childJSON in children {
                        let redditEntity = RedditEntity(withJSON: childJSON)
                        result.append(redditEntity)
                    }
                
                    let paginator = Paginator(withResponceDictionary: jsonDictionary)
                    return .success((result, paginator))
                }
            }
        }
        
        return .failure(RedditError.unableToParseJSON(json) as NSError)
    }
}


