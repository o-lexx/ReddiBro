//
//  RedditParser.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Foundation

class RedditParser {
    func parse(dictionary json: JSONDictionary, kind: String) -> Any? {
        if let data = json["data"] as? JSONDictionary {
            if let children = data["children"] as? Array<JSONDictionary> {
                var result: Array<RedditEntity> = []
                for childJSON in children {
                    let redditEntity = RedditEntity.init(withJSON: childJSON)
                    result.append(redditEntity)
                }
                
                return result
            }
        }
        return nil
    }
}
