//
//  HTTPResponse.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Foundation

struct Response {
    let data: Data
    let statusCode: Int
    let urlResponse: URLResponse?
    
    init(data: Data?, urlResponse: URLResponse?) {
        if let data = data {
            self.data = data
        } else {
            self.data = Data()
        }
        self.urlResponse = urlResponse
        
        if let httpResponse = urlResponse as? HTTPURLResponse {
            statusCode = httpResponse.statusCode
        } else {
            statusCode = 500
        }
    }
}
