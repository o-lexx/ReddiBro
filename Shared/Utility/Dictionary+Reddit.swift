//
//  Dictionary+Reddit.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Foundation

extension String {
    var addUrlPercentEncoding: String {
        get {
            return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? self
        }
    }
}

extension Dictionary where Key == String, Value == String {
    var URLQuery: String {
        get {
            return self.map({ "\($0.addUrlPercentEncoding)=\($1.addUrlPercentEncoding)" }).joined(separator: "&")
        }
    }
    
    mutating func merge(with dictionary: Dictionary) {
        for (key, value) in dictionary {
            self[key] = value
        }
    }
}


