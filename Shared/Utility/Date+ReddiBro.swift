//
//  Date+ReddiBro.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Foundation

extension Date {
    var localizedTimePeriodFromNow: String {
        get {
            /*
             I have no time wo write 'nice' implementation, 
             this one works fine for test
             */
            
            let seconds = Int(Date().timeIntervalSince(self))
            let min = seconds / 60
            let hours = min / 60
            let days = hours / 24
            let years = days / 365
            
            if (years > 0) {
                return "\(years) year(s) ago"
            }
            if (days > 0) {
                return "\(days) day(s) ago"
            }
            if (hours > 0) {
                return "\(hours) hour(s) ago"
            }
            
            return "Just now"
        }
    }
}
